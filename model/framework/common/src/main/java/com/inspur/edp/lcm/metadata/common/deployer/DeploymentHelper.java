/*
 *  Copyright © OpenAtom Foundation.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.inspur.edp.lcm.metadata.common.deployer;

import com.inspur.edp.lcm.metadata.api.entity.deployer.DeploymentType;
import com.inspur.edp.lcm.project.deployer.spi.DeployAction;
import java.util.List;

public class DeploymentHelper extends DeploymentTypeConfigLoader {

    private static DeploymentHelper singleton = null;

    public DeploymentHelper() {
    }

    public static DeploymentHelper getInstance() {
        if (singleton == null) {
            singleton = new DeploymentHelper();
        }
        return singleton;
    }

    public List<DeploymentType> getDeploymentType() {
        return loadDeploymentTypeConfiguration();
    }

    public DeployAction getManager(String typeCode) {
        DeployAction manager = null;
        DeploymentType deploymentType = getDeploymentType(typeCode);
        if (deploymentType != null) {
            Class<?> cls;
            try {
                if (deploymentType.getDeployer() != null) {
                    cls = Class.forName(deploymentType.getDeployer().getName());
                    manager = (DeployAction) cls.newInstance();
                }
            } catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
                e.printStackTrace();
                throw new RuntimeException(e);
            }
        }
        return manager;
    }
}
