/*
 *  Copyright © OpenAtom Foundation.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.inspur.edp.lcm.metadata.api.service;

import java.io.IOException;
import java.util.List;
import com.inspur.edp.lcm.metadata.api.entity.GspProject;
import com.inspur.edp.lcm.metadata.api.ConfigData.gspproject.ProjectConfiguration;

/**
 * GSP Project工程服务
 */
public interface GspProjectService {

    /**
     * 获取工程信息（GSPProject.json的信息）
     *
     * @param path 工程路径
     * @return 工程路径
     * @throws IOException
     */

    GspProject getGspProjectInfo(String path) throws IOException;

    List<ProjectConfiguration> getGspProjectTypeInfo();
}
