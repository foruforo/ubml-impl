/*
 *  Copyright © OpenAtom Foundation.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.inspur.edp.lcm.metadata.core.manager;

import com.inspur.edp.lcm.metadata.api.mvnEntity.MavenPackageRefs;
import com.inspur.edp.lcm.metadata.common.MavenUtils;
import com.inspur.edp.lcm.metadata.core.persistence.RepositoryFactory;
import java.util.List;
import org.apache.maven.model.DistributionManagement;
import org.apache.maven.model.Model;

public class PomManager {

    public void createPomWithSourceModel(Model sourceModel, String pomPath, String repoId, String dependencyVersion) {
        final Model model = createModelWithSourceModel(sourceModel, repoId, dependencyVersion);
        writeModel(pomPath, model);
    }

    public void createPomForDependencyCopy(List<MavenPackageRefs> refs, String pomPath) {
        RepositoryFactory.getInstance().getPomRepository().createPomForDependencyCopy(refs, pomPath);
    }

    public void updatePom(String pomPath, List<MavenPackageRefs> refs, String packaging) {
        RepositoryFactory.getInstance().getPomRepository().updatePom(pomPath, refs, packaging);
    }

    public void setRepoUrl(String pomPath, String repoId) {
        final Model model = getModel(pomPath);
        final DistributionManagement distributionManagement = RepositoryFactory.getInstance().getMavenSettingsRepository().getDistributionManagement(repoId, model.getVersion().endsWith(MavenUtils.SNAP_SHOT));
        model.setDistributionManagement(distributionManagement);
        writeModel(pomPath, model);
    }

    public void writeModel(String pomPath, Model model) {
        RepositoryFactory.getInstance().getPomRepository().writeModel(pomPath, model);
    }

    public Model getModel(String pomPath) {
        Model model = RepositoryFactory.getInstance().getPomRepository().getModel(pomPath);
        model.setGroupId(model.getGroupId() == null ? model.getParent().getGroupId() : model.getGroupId());
        model.setArtifactId(model.getArtifactId() == null ? model.getParent().getArtifactId() : model.getArtifactId());
        model.setVersion(model.getVersion() == null ? model.getParent().getVersion() : model.getVersion());
        return model;
    }

    private Model createModelWithSourceModel(Model sourceModel, String repoId, String dependencyVersion) {
        final Model modelWithSourceModel = RepositoryFactory.getInstance().getPomRepository().createModelWithSourceModel(sourceModel, repoId, dependencyVersion);
        return modelWithSourceModel;
    }

}
