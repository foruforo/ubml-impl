/*
 *  Copyright © OpenAtom Foundation.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package org.openatom.ubml.model.common.definition.cef.json.Variable;

import com.fasterxml.jackson.core.JsonParser;
import org.openatom.ubml.model.common.definition.cef.collection.GspFieldCollection;
import org.openatom.ubml.model.common.definition.cef.entity.GspCommonField;
import org.openatom.ubml.model.common.definition.cef.json.CefNames;
import org.openatom.ubml.model.common.definition.cef.json.SerializerUtils;
import org.openatom.ubml.model.common.definition.cef.json.element.CefFieldDeserializer;
import org.openatom.ubml.model.common.definition.cef.variable.CommonVariable;
import org.openatom.ubml.model.common.definition.cef.variable.CommonVariableCollection;
import org.openatom.ubml.model.common.definition.cef.variable.VariableSourceType;

/**
 * The Json Parser Of Common Variable
 *
 * @ClassName: CommonVariableDeserializer
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class CommonVariableDeserializer extends CefFieldDeserializer {
    @Override
    protected boolean readExtendFieldProperty(GspCommonField item, String propName, JsonParser jsonParser) {
        boolean result = true;
        CommonVariable field = (CommonVariable)item;
        switch (propName) {
            case CefNames.VARIABLE_SOURCE_TYPE:
                field.setVariableSourceType(SerializerUtils
                        .readPropertyValue_Enum(jsonParser, VariableSourceType.class, VariableSourceType.values(), VariableSourceType.BE));
                break;
            default:
                result = false;
        }
        return result;
    }

    @Override
    protected GspFieldCollection CreateFieldCollection() {
        return new CommonVariableCollection(null);
    }

    @Override
    protected GspCommonField CreateField() {
        return new CommonVariable();
    }
}
