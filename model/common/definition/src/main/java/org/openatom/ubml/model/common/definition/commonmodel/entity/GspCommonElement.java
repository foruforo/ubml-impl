/*
 *  Copyright © OpenAtom Foundation.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package org.openatom.ubml.model.common.definition.commonmodel.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.openatom.ubml.model.common.definition.cef.collection.GspAssociationCollection;
import org.openatom.ubml.model.common.definition.cef.element.GspAssociation;
import org.openatom.ubml.model.common.definition.cef.entity.GspCommonField;
import org.openatom.ubml.model.common.definition.commonmodel.IGspCommonElement;
import org.openatom.ubml.model.common.definition.commonmodel.IGspCommonObject;
import org.openatom.ubml.model.common.definition.commonmodel.entity.element.ElementCodeRuleConfig;
import org.openatom.ubml.model.common.definition.commonmodel.entity.element.GspCommonAssociation;

/**
 * The Definition Of Common Element
 *
 * @ClassName: GspCommonElement
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class GspCommonElement extends GspCommonField implements IGspCommonElement {
    // 基础信息
    private transient ElementCodeRuleConfig billCodeConfig = new ElementCodeRuleConfig();

    // 成员字段
    @JsonIgnore
    private transient String columnid = "";
    private transient String belongModelId = "";
    private transient boolean readOnly;
    // 维护信息
    // private String customExpression = "";
    private boolean isCustomItem;

    public GspCommonElement() {
        billCodeConfig = new ElementCodeRuleConfig();
        setReadonly(false);
        setIsCustomItem(false);
    }

    /**
     * 编码规则配置
     */
    @Override
    public ElementCodeRuleConfig getBillCodeConfig() {
        return billCodeConfig;
    }

    public void setBillCodeConfig(ElementCodeRuleConfig value) {
        billCodeConfig = value;
    }

    /**
     * 对应的数据对象的列ID
     */
    @Override
    public String getColumnID() {
        return columnid;
    }

    @Override
    public void setColumnID(String value) {
        columnid = value;
    }

    /**
     * 是否自定义项
     */
    @Override
    public boolean getIsCustomItem() {
        return isCustomItem;
    }

    @Override
    public void setIsCustomItem(boolean value) {
        this.isCustomItem = value;
    }

    /**
     * 当前属性所属结点
     */
    @Override
    public IGspCommonObject getBelongObject() {
        return (IGspCommonObject)super.getBelongObject();
    }

    @Override
    public void setBelongObject(IGspCommonObject value) {
        super.setBelongObject(value);
    }

    /**
     * 所属数据模型metadataId
     */
    @Override
    public String getBelongModelID() {
        return belongModelId;
    }

    @Override
    public void setBelongModelID(String value) {
        value = belongModelId;
    }

    @Override
    public boolean getReadonly() {
        return readOnly;
    }

    @Override
    public void setReadonly(boolean value) {
        readOnly = value;
    }


    /**
     * 克隆
     *
     * @param absObj
     * @param association
     * @return
     */
    @Override
    public final IGspCommonElement clone(IGspCommonObject absObj, GspCommonAssociation association) {
        Object tempVar = super.clone(absObj, association);
        GspCommonElement newObj = (GspCommonElement)((tempVar instanceof GspCommonElement) ? tempVar : null);
        if (newObj == null) {
            return null;
        }
        newObj.setBelongObject(absObj);
        newObj.setParentAssociation(association);
        if (getChildAssociations() != null) {
            newObj.setChildAssociations(new GspAssociationCollection());
            for (GspAssociation item : getChildAssociations()) {
                newObj.getChildAssociations().add(((GspCommonAssociation)item).clone(newObj));
            }
        }
        return newObj;
    }

    public final GspCommonAssociation getGspParentAssociation() {
        return (GspCommonAssociation)getParentAssociation();
    }

    public final void setGspParentAssociation(GspCommonAssociation value) {
        setParentAssociation(value);
    }

    @Override
    @JsonIgnore
    public final String getAssociationTypeName() {
        if (getBelongObject().getParentObject() == null) {
            return getLabelID() + "Info";
        }
        return getBelongObject().getCode() + getLabelID() + "Info";
    }

    @Override
    @JsonIgnore
    public final String getEnumTypeName() {
        if (getBelongObject().getParentObject() == null) {
            return getLabelID() + "Enum";
        }
        return getBelongObject().getCode() + getLabelID() + "Enum";
    }

}