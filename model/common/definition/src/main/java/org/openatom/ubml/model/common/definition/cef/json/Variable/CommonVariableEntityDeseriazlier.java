/*
 *  Copyright © OpenAtom Foundation.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package org.openatom.ubml.model.common.definition.cef.json.Variable;

import org.openatom.ubml.model.common.definition.cef.collection.GspFieldCollection;
import org.openatom.ubml.model.common.definition.cef.entity.GspCommonDataType;
import org.openatom.ubml.model.common.definition.cef.json.element.CefFieldDeserializer;
import org.openatom.ubml.model.common.definition.cef.json.object.GspCommonDataTypeDeserializer;
import org.openatom.ubml.model.common.definition.cef.variable.CommonVariableCollection;
import org.openatom.ubml.model.common.definition.cef.variable.CommonVariableEntity;

/**
 * The Json Parser Of Common Variable Entity
 *
 * @ClassName: CommonVariableEntityDeseriazlier
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class CommonVariableEntityDeseriazlier extends GspCommonDataTypeDeserializer {
    @Override
    protected GspCommonDataType createCommonDataType() {
        return new CommonVariableEntity();
    }

    @Override
    protected CefFieldDeserializer createFieldDeserializer() {
        return new CommonVariableDeserializer();
    }

    @Override
    protected GspFieldCollection createFieldCollection() {
        return new CommonVariableCollection((CommonVariableEntity)getGspCommonDataType());
    }
}
